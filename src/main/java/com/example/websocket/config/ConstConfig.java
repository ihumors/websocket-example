package com.example.websocket.config;

public class ConstConfig {
    public static final String ONLINE_USER_KEY = "WEBSOCKET:ONLINE-USERS";
    public static final String CHANNEL_TOPIC_KEY = "WEBSOCKET:CHANNEL_TOPIC";
    public static final String MSG_TO_ALL_KEY = "WEBSOCKET:MSG-TO-ALL";
}
