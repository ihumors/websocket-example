package com.example.websocket;

import com.alibaba.fastjson2.JSON;
import com.example.websocket.model.ChatMessage;
import org.junit.jupiter.api.Test;

public class JSONTests {
    @Test
    public void jsonTest(){
        String msg="{\"sender\":\"PD\",\"type\":1}";
        ChatMessage chatMessage = JSON.parseObject(msg, ChatMessage.class);
        System.out.println(chatMessage);
    }
}
